# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive():
    age = int(input("Enter your age: "))
    has_consent = input("Do you have a signed consent form? (yes/no): ")

    if age >= 18:
        print("You may skydive!")
    elif age < 18 and has_consent.lower() == "yes":
        print("You may skydive!")
    else:
        print("Sorry, you may not skydive.")
    
can_skydive()