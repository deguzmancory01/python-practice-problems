# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    reversed_word = ''.join(reversed(word))
    return word == reversed_word

# Prompt the user for a word
user_word = input("Enter a word: ")

# Check if the word is a palindrome
if is_palindrome(user_word):
    print("The word is a palindrome.")
else:
    print("The word is not a palindrome.")
