# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if not values or len(values) < 2:
        return None

    largest = max(values)
    values.remove(largest)
    second_largest = max(values)
    return second_largest

print(find_second_largest([10, 9, 8, 11, 12]))