# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(word):
    shifted_word = ""
    for letter in word:
        if letter.isalpha():
            if letter == "Z":
                shifted_letter = "A"
            elif letter == "z":
                shifted_letter = "a"
            else:
                shifted_letter = chr(ord(letter) + 1)
        else:
            shifted_letter = letter
        shifted_word += shifted_letter
    return shifted_word


print(shift_letters("import"))    # Output: "jnqpsu"
print(shift_letters("ABBA"))      # Output: "BCCB"
print(shift_letters("Kala"))      # Output: "Lbmb"
print(shift_letters("zap"))       # Output: "abq"
